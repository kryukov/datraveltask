<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class MaintenanceGroup extends Model
{
    protected $fillable = [
        'name',
    ];

    protected $with = [
        'companies',
    ];

    /**
     * @return HasMany
     * Получить список компаний
     */
    public function companies(): HasMany
    {
        return $this->hasMany(Company::class, 'maintenance_group_id');
    }
}
