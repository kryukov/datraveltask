<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Company extends Model
{
    protected $fillable = [
        'name',
    ];

    protected $with = [
        'departments',
    ];

    /**
     * @return HasMany
     * Получить список отделений
     */
    public function departments(): HasMany
    {
        return $this->hasMany(Department::class, 'company_id');
    }
}
