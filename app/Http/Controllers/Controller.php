<?php

namespace App\Http\Controllers;

use App\Models\MaintenanceGroup;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @return Collection
     * Для отладки
     */
    protected function getMaintenanceGroup(): Collection
    {
        return MaintenanceGroup::all();
    }
}
